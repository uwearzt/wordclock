# ------------------------------------------------------------------------------
# Copyright 2023 Uwe Arzt, mail@uwe-arzt.de
# SPDX-License-Identifier: GPL-3.0-or-later
# ------------------------------------------------------------------------------

import os
import time
from colorama import init, Fore, Style
from collections import namedtuple
from datetime import datetime

Word = namedtuple('Word', ['off', 'len'])

# word definitions
es          = Word(off=0, len=2)
ist         = Word(off=3, len=3)
vor         = Word(off=33, len=3)
funk        = Word(off=36, len=4)
nach        = Word(off=40, len=4)
uhr         = Word(off=107, len=3)

# minute definitions
fuenf       = Word(off=7, len=4)
zehn        = Word(off=11, len=4)
viertel     = Word(off=26, len=7)
zwanzig     = Word(off=15, len=7)
halb        = Word(off=44, len=4)
dreiviertel = Word(off=22, len=11)

# hour definitions
h_ein       = Word(off=55, len=3)
h_zwei      = Word(off=62, len=4)
h_drei      = Word(off=66, len=4)
h_vier      = Word(off=73, len=4)
h_fuenf     = Word(off=51, len=4)
h_sechs     = Word(off=77, len=5)
h_sieben    = Word(off=88, len=6)
h_acht      = Word(off=84, len=4)
h_neun      = Word(off=102, len=4)
h_zehn      = Word(off=99, len=4)
h_elf       = Word(off=49, len=3)
h_zwoelf    = Word(off=94, len=5)

# create a array with hour definitions
hours = [ h_ein, h_zwei, h_drei, h_vier, h_fuenf, h_sechs,
          h_sieben, h_acht, h_neun, h_zehn, h_elf, h_zwoelf]

# The characters are organized on the Display in 11 columns and 10 rows.
display_characters = [
     'E','S','k','I','S','T','a','F','Ü','N','F',
     'Z','E','H','N','Z','W','A','N','Z','I','G',
     'D','R','E','I','V','I','E','R','T','E','L',
     'V','O','R','F','U','N','K','N','A','C','H',
     'H','A','L','B','a','E','L','F','Ü','N','F',
     'E','I','N','S','x','a','m','Z','W','E','I',
     'D','R','E','I','a','u','j','V','I','E','R',
     'S','E','C','H','S','n','l','A','C','H','T',
     'S','I','E','B','E','N','Z','W','Ö','L','F',
     'Z','E','H','N','E','U','N','k','U','H','R'
]

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

def print_led(led):
    led_dim = Style.DIM + "o" + Style.RESET_ALL
    led_light = Style.BRIGHT + Fore.RED + "o" + Style.RESET_ALL

    if led is True:
        print(led_light, end='')
    else:
        print(led_dim, end='')


def display(char_leds, min_leds):
    print_led(min_leds[0])
    print("                       ", end='')
    print_led(min_leds[1])
    print("")

    for row in range(10):
        print(" ", end = '')
        for column in range(11):
            pos = (row*11) + column
            if char_leds[pos] == True:
                print(Style.BRIGHT + Fore.RED +" " + display_characters[pos] + Style.RESET_ALL, end = '')
            else:
                print(" " + Style.DIM + display_characters[pos] + Style.RESET_ALL, end = '')
        print("")

    print_led(min_leds[2])
    print("                       ", end='')
    print_led(min_leds[3])
    print("")

def set_char_leds(char_leds, word):
     start = word.off
     end   = word.off + word.len
     for led in range(start, end): 
         char_leds[led] = True

def  decrement_hour(hour):
    if hour == 0:
        return 11
    else:
        return hour - 1

def main():
    init()

    while True:
        char_leds = [False] * 110
        min_leds = [False] * 4

        now = datetime.now()
        hour = int(now.strftime('%I'))
        minute = now.minute

        clear()

        # print("time: " + str(hour) + ":" + str(minute))
        
        set_char_leds(char_leds, es)
        set_char_leds(char_leds, ist)
        set_char_leds(char_leds, uhr)

        # minutes
        if 0 <= minute <= 4:
            hour = decrement_hour(hour)
        elif 5 <= minute <= 9:
            set_char_leds(char_leds, fuenf)
            set_char_leds(char_leds, nach)
            hour = decrement_hour(hour)
        elif 10 <= minute <= 14:
            set_char_leds(char_leds, zehn)
            set_char_leds(char_leds, nach)
            hour = decrement_hour(hour)
        elif 15 <= minute <= 19:
            set_char_leds(char_leds, viertel)
            set_char_leds(char_leds, nach)
            hour = decrement_hour(hour)
        elif 20 <= minute <= 24:
            set_char_leds(char_leds, zwanzig)
            set_char_leds(char_leds, nach)
            hour = decrement_hour(hour)
        elif 25 <= minute <= 29:
            set_char_leds(char_leds, fuenf)
            set_char_leds(char_leds, vor)
            set_char_leds(char_leds, halb)
        elif 30 <= minute <= 34:
            set_char_leds(char_leds, halb)
        elif 35 <= minute <= 39:
            set_char_leds(char_leds, fuenf)
            set_char_leds(char_leds, nach)
            set_char_leds(char_leds, halb)
        elif 40 <= minute <= 44:
            set_char_leds(char_leds, zwanzig)
            set_char_leds(char_leds, vor)
        elif 45 <= minute <= 49:
            set_char_leds(char_leds, dreiviertel)
        elif 50 <= minute <= 54:
            set_char_leds(char_leds, zehn)
            set_char_leds(char_leds, vor)
        elif 55 <= minute <= 59:
            set_char_leds(char_leds, fuenf)
            set_char_leds(char_leds, vor)
        else:
            print("should never happen")

        # minute leds
        leds = minute % 5
        for led in range(0, leds):
            min_leds[led] = True

        # hour
        set_char_leds(char_leds, hours[hour])

        display(char_leds, min_leds)

        time.sleep(0.5)

if __name__ == '__main__':
    main()