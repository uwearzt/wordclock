# WordClock

WordClock implementation.

## Contributors

* mail@uwe-arzt.de Uwe Arzt

## License

GNU General Public License, Version 3.0 [LICENSE](LICENSE) or
[https://spdx.org/licenses/GPL-3.0-or-later.html](https://spdx.org/licenses/GPL-3.0-or-later.html)